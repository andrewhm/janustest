# janustest

Тестовый пример реализации взаимодействия бэкенда с janusgraph

**Создание тестовой структуры**  
В корне проекта находится файл testStructure.http (описание запросов для http клиента idea), с помощью которого можно создать тестовую структуру.  
   1. Запустить приложение, после чего будет создана тестовая схема  
   2. Открыть файл testStructure.http и запустить на выполнение все запросы  
   3. Убедиться, что запросы вернули код 200  

**method:** POST  
**comment:** create employee  
**url:** http://localhost:8080/employee  
**json:**
```json
{
    "name": "Иванов Иван Иванович",
    "position": "Несущий разработчик",
    "department": "Отдел разработки программного обеспечения",
    "city": "Москва"
}
```
-------------------------------------------------------------------------------------

**method:** GET  
**comment:** get employee  
**url:** http://localhost:8080/employee?name=Иванов

-------------------------------------------------------------------------------------

**method:** PATCH  
**comment:** update employee  
**url:** http://localhost:8080/employee  
**json:**
```json
{
    "name": "Иванов Иван Иванович",
    "position": "Несущий разработчик33",
    "department": "Отдел разработки программного обеспечения33",
    "city": "Москва33"
}
```
-------------------------------------------------------------------------------------

**method:** GET  
**comment:** list of skills by employee  
**url:** http://localhost:8080/employee/4248/skills 

-------------------------------------------------------------------------------------

**method:** GET  
**commet:** get all employees with inner objects of contacts  
**url:** http://localhost:8080/employee/with_contacts  

-------------------------------------------------------------------------------------

**method:** DELETE  
**comment:** delete employee  
**url:** http://localhost:8080/employee?id=8320

-------------------------------------------------------------------------------------

**method:** POST  
**comment:** create skill  
**url:** http://localhost:8080/skill  
**json:**
```json
{
    "title": "Java",
    "description": "Java info"
}
```
-------------------------------------------------------------------------------------

**method:** GET  
**comment:** get skill  
**url:** http://localhost:8080/skill?title=Java  

-------------------------------------------------------------------------------------

**method:** PATCH  
**comment:** update skill  
**url:** http://localhost:8080/skill  
**json:**
```json
{
    "title": "Java",
    "description": "Java info33"
}
```
-------------------------------------------------------------------------------------

**method:** DELETE  
**comment:** delete skill  
**url:** http://localhost:8080/skill?id=4425

-------------------------------------------------------------------------------------

**method:** PUT  
**commet:** link skill to employee  
**url:** http://localhost:8080/skill/Java  
**json:**
```json
{
    "name": "Иванов Иван Иванович",
    "position": "Несущий разработчик33",
    "department": "Отдел разработки программного обеспечения33",
    "city": "Москва33"
}
```
-------------------------------------------------------------------------------------

**method:** POST  
**commet:** create contact  
**url:** http://localhost:8080/contact  
**json:**
```json
{
    "email": "i.ivanov@cinimex.ru",
    "work_phone": "+7 (495) 755-55-55",
    "mobile_phone": "+7 900 182-22-11"
}
```
-------------------------------------------------------------------------------------

**method:** PUT  
**commet:** link contact to employee  
**url:** http://localhost:8080/contact/i.ivanov@cinimex.ru  
**json:**
```json
{
    "name": "Иванов Иван Иванович",
    "position": "Несущий разработчик",
    "department": "Отдел разработки программного обеспечения",
    "city": "Москва"
}
```
-------------------------------------------------------------------------------------