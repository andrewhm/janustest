package com.janusprototype.janustest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JanustestApplication {

	public static void main(String[] args) {
		SpringApplication.run(JanustestApplication.class, args);
	}

}
