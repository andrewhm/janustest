package com.janusprototype.janustest.config;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Cluster;
import org.apache.tinkerpop.gremlin.driver.remote.DriverRemoteConnection;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.apache.tinkerpop.gremlin.process.traversal.AnonymousTraversalSource.traversal;

/*
 * Конфигурация для подключения к Gremlin серверу
 */
@Configuration
public class MainConfiguration {

    private static String SOURCE_NAME = "g";
    private static String REMOTE_CONNECT_FILE_PATH = "src/main/resources/conf/remote-objects.yaml";

    @Bean(destroyMethod = "close")
    public Cluster cluster() throws Exception {
        return Cluster.open(REMOTE_CONNECT_FILE_PATH);
    }

    @Bean
    public Client gremlinClient(Cluster cluster) {
        return cluster.connect();
    }

    @Bean(destroyMethod = "close")
    public GraphTraversalSource graphTraversalSource(Cluster cluster) throws ConfigurationException {
        return traversal().withRemote(DriverRemoteConnection.using(cluster, SOURCE_NAME));
    }
}
