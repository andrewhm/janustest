package com.janusprototype.janustest.controller;

import com.janusprototype.janustest.domain.Contacts;
import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.repositories.ContactRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("contact")
public class ContactController {
    private final ContactRepository contactRepository;

    @PostMapping
    public ResponseEntity<Boolean> create(@RequestBody Contacts contacts) {
        boolean result = contactRepository.create(contacts);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @GetMapping
    public ResponseEntity<Contacts> findContactById(@RequestParam Long id) {
        Contacts contacts = contactRepository.findContactById(id);
        return contacts == null  ? new ResponseEntity("data wasn't found in db", NOT_FOUND) : new ResponseEntity(contacts, OK);
    }

    @DeleteMapping
    public ResponseEntity<Boolean> remove(@RequestParam Long id) {
        boolean result = contactRepository.remove(id);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @PatchMapping
    public ResponseEntity<Boolean> udpateContactByEmail(@RequestBody Contacts contact) {
        boolean result = contactRepository.update(contact);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @PutMapping("{email}")
    public ResponseEntity<Boolean> addContactEmployees(@PathVariable("email") String email, @RequestBody Employee employee) {
        boolean result = contactRepository.addContactToEmployee(email, employee);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }
}
