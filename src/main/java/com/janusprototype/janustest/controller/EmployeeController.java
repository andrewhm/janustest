package com.janusprototype.janustest.controller;

import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.domain.Skill;
import com.janusprototype.janustest.repositories.EmployeeRepository;
import com.janusprototype.janustest.repositories.filter.FilterEmployeeByNameCityPosition;
import com.janusprototype.janustest.repositories.filter.FilterMap;
import lombok.RequiredArgsConstructor;
import org.apache.tinkerpop.gremlin.process.traversal.P;
import org.apache.tinkerpop.gremlin.process.traversal.TextP;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping("employee")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeRepository employeeRepository;

    @PostMapping
    public ResponseEntity<Boolean> create(@RequestBody Employee employee) {
        boolean result = employeeRepository.create(employee);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @GetMapping
    public ResponseEntity<Employee> findEmployeeByName(@RequestParam String name) {
        Employee employee = employeeRepository.findEmployeeByName(name);
        return employee == null ? new ResponseEntity("data wasn't found in db", NOT_FOUND) : new ResponseEntity(employee, OK);
    }

    @DeleteMapping
    public ResponseEntity<Boolean> remove(@RequestParam Long id) {
        boolean result = employeeRepository.remove(id);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @PatchMapping
    public ResponseEntity<Boolean> udpateEmployeeByName(@RequestBody Employee employee) {
        boolean result = employeeRepository.update(employee);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @GetMapping("{id}/skills")
    public List<Skill> skillList(@PathVariable("id") Long id) {
        return employeeRepository.listSkills(id);
    }

    @GetMapping("/with_contacts")
    public ResponseEntity<List<Employee>> findEmployeesWithContacts() {
        List<Employee> employees = employeeRepository.findEmployeesWithContacts();
        return employees == null || employees.isEmpty() ? new ResponseEntity("data wasn't found in db", NOT_FOUND) : new ResponseEntity(employees, OK);
    }

    @GetMapping("/with_parameters_map")
    public ResponseEntity<List<Employee>> findEmployeesByParametersMap() {
        FilterMap filterMap = new FilterMap();
        filterMap.setParameter("name", Arrays.asList(TextP.containing("Jack"), TextP.containing("Иван")));
        filterMap.setParameter("city", Arrays.asList(P.eq("Москва")));
        filterMap.setParameter("position", Arrays.asList(TextP.containing("developer")));

        List<Employee> employees = employeeRepository.listBy(filterMap);
        return employees == null || employees.isEmpty() ? new ResponseEntity("data wasn't found in db", NOT_FOUND) : new ResponseEntity(employees, OK);
    }

    @GetMapping("/with_filter_strategy")
    public ResponseEntity<List<Employee>> findEmployeesByFilterStrategy() {
        List<Employee> employees = employeeRepository.listBy(new FilterEmployeeByNameCityPosition());
        return employees == null || employees.isEmpty() ? new ResponseEntity("data wasn't found in db", NOT_FOUND) : new ResponseEntity(employees, OK);
    }
}
