package com.janusprototype.janustest.controller;

import com.janusprototype.janustest.domain.Contacts;
import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.domain.Skill;
import com.janusprototype.janustest.repositories.ContactRepository;
import com.janusprototype.janustest.repositories.SkillRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;
import static org.springframework.http.HttpStatus.OK;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("skill")
public class SkillController {
    private final SkillRepository skillRepository;

    @PostMapping
    public ResponseEntity<Boolean> create(@RequestBody Skill skill) {
        boolean result = skillRepository.create(skill);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @GetMapping
    public ResponseEntity<Skill> findSkillByTitle(@RequestParam String title) {
        Skill skill = skillRepository.findSkillByTitle(title);
        return skill == null  ? new ResponseEntity("data wasn't found in db", NOT_FOUND) : new ResponseEntity(skill, OK);
    }

    @DeleteMapping
    public ResponseEntity<Boolean> remove(@RequestParam Long id) {
        boolean result = skillRepository.remove(id);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @PatchMapping
    public ResponseEntity<Boolean> udpateSkillByTitle(@RequestBody Skill skill) {
        boolean result = skillRepository.update(skill);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

    @PutMapping("{title}")
    public ResponseEntity<Boolean> addSkillEmployees(@PathVariable("title") String title, @RequestBody Employee employee) {
        boolean result = skillRepository.addSkillToEmployee(title, employee);
        return new ResponseEntity(result, result ? HttpStatus.OK : INTERNAL_SERVER_ERROR);
    }

}
