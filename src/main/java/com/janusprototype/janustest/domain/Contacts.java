package com.janusprototype.janustest.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"email"})
@Builder
public class Contacts {

    private Long id;

    @Email
    @NotBlank
    private String email;

    @JsonProperty("work_phone")
    private String workPhone;

    @JsonProperty("mobile_phone")
    private String mobilePhone;

}
