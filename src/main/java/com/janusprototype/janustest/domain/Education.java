package com.janusprototype.janustest.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

/**
 * Employee education.
 */
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@JsonFilter("education")
@RequiredArgsConstructor
public class Education {

    @NotBlank
    @JsonProperty("education_institution")
    private final String educationInstitution;

    @NotBlank
    private final String specialization;

    @NotBlank
    private final String degree;

    @Past
    @NotNull
    @JsonProperty("started_at")
    private final LocalDate startedAt;

    @Past
    @JsonProperty("ended_at")
    private LocalDate endedAt;
}
