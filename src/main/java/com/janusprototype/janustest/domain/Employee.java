package com.janusprototype.janustest.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode(of = {"name", "position", "city", "department"})
@Builder
public class Employee {

    private Long id;
    @NotBlank
    private String name;
    @NotBlank
    private String position;
    @NotBlank
    private String department;
    @NotBlank
    private String city;

    private Contacts contacts;
    private List<Education> educations;
    private List<Study> studies;

    @Past
    @Setter
    @JsonProperty("birth_date_at")
    private LocalDate birthDate;

    // bullshit
    private String bullshit;

}
