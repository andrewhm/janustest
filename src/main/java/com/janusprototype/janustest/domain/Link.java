package com.janusprototype.janustest.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import java.beans.ConstructorProperties;
import java.net.URI;

@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor(onConstructor_ = {@ConstructorProperties({"title", "uri"})})
public class Link {

    private final String title;
    private final URI uri;

}
