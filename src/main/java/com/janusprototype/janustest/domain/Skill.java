package com.janusprototype.janustest.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.validation.constraints.NotBlank;
import java.beans.ConstructorProperties;
import java.util.List;
import java.util.Objects;

@Getter
@ToString
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Skill {

    private Long id;
    @NotBlank
    private String title;
    private String description;
    @JsonProperty("related_resources")
    private List<Link> relatedResources;

    @ConstructorProperties({"title", "description"})
    public Skill(@Nonnull String title, @Nullable String description) {
        this.title = Objects.requireNonNull(title);
        this.description = description;
    }
}
