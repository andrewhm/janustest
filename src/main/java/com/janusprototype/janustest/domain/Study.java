package com.janusprototype.janustest.domain;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

/**
 * Employee additional education.
 */
@Getter
@ToString
@EqualsAndHashCode
@JsonFilter("study")
@RequiredArgsConstructor
public class Study {

    @NotBlank
    private final String title;

    @Past
    @NotNull
    @JsonProperty("passed_at")
    private final LocalDate passedAt;

    /*@NotNull
    private final StudyType type;*/
}
