package com.janusprototype.janustest.init;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.driver.Client;
import org.apache.tinkerpop.gremlin.driver.Result;
import org.apache.tinkerpop.gremlin.driver.ResultSet;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.stream.Stream;

import static com.janusprototype.janustest.utils.Utils.createSchemaRequest;

@Component
@Slf4j
@RequiredArgsConstructor
public class InitConfig {

    private final Client client;

    /*
     * Создание схемы производится путем отправки скрипта, описывающего схему, на сервер gremlin c помощью клиента
     */
    @PostConstruct
    private void initSchema() {
        log.info("creating schema");
        final String req = createSchemaRequest();
        final ResultSet resultSet = client.submit(req);
        Stream<Result> futureList = resultSet.stream();
        futureList.map(Result::toString).forEach(log::info);
    }
}
