package com.janusprototype.janustest.mapper;

import com.janusprototype.janustest.domain.Contacts;
import com.janusprototype.janustest.utils.MappingUtil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Mapper(uses = MappingUtil.class)
public interface ContactMapper {

    ContactMapper MAPPER = Mappers.getMapper(ContactMapper.class);

    @Mapping(source = "map", target = "id", qualifiedBy = MappingUtil.Id.class)
    @Mapping(source = "map", target = "email", qualifiedBy = MappingUtil.Email.class)
    @Mapping(source = "map", target = "mobilePhone", qualifiedBy = MappingUtil.MobilePhone.class)
    @Mapping(source = "map", target = "workPhone", qualifiedBy = MappingUtil.WorkPhone.class)
    Contacts toContacts(Map<Object, Object> map);
}
