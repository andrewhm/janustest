package com.janusprototype.janustest.mapper;

import com.janusprototype.janustest.domain.Contacts;
import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.utils.MappingUtil;
import org.apache.tinkerpop.gremlin.structure.T;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Mapper(uses = MappingUtil.class)
public interface EmployeeMapper {
    EmployeeMapper MAPPER = Mappers.getMapper(EmployeeMapper.class);

    String EMPLOYEE_LABEL = "employee";

    default Employee toEmployeeWithInnerEntities(List<Map<Object, Object>> mapList) {
        if (mapList == null || mapList.isEmpty()) {
            return null;
        }

        Employee employee = null;
        Contacts contact = null;

        for(Map<Object, Object> map: mapList) {
            if (isEmployee(map)) {
                employee = toEmployee(map);
            } else {
                contact = ContactMapper.MAPPER.toContacts(map);
            }
        }

        if (employee != null) {
            employee.setContacts(contact);
        }

        return employee;
    }

    @Mapping(source = "map", target = "id", qualifiedBy = MappingUtil.Id.class)
    @Mapping(source = "map", target = "name", qualifiedBy = MappingUtil.Name.class)
    @Mapping(source = "map", target = "position", qualifiedBy = MappingUtil.Position.class)
    @Mapping(source = "map", target = "department", qualifiedBy = MappingUtil.Department.class)
    @Mapping(source = "map", target = "city", qualifiedBy = MappingUtil.City.class)
    Employee toEmployee(Map<Object, Object> map);

    default boolean isEmployee(Map<Object, Object> map) {
        return Optional.ofNullable(map)
                .map(mapLocal -> EMPLOYEE_LABEL.equals(map.get(T.label).toString()))
                .orElse(false);
    }
}
