package com.janusprototype.janustest.mapper;

import com.janusprototype.janustest.domain.Skill;
import com.janusprototype.janustest.utils.MappingUtil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.Map;

@Mapper(uses = MappingUtil.class)
public interface SkillMapper {
    SkillMapper MAPPER = Mappers.getMapper(SkillMapper.class);

    @Mapping(source = "map", target = "id", qualifiedBy = MappingUtil.Id.class)
    @Mapping(source = "map", target = "title", qualifiedBy = MappingUtil.Title.class)
    @Mapping(source = "map", target = "description", qualifiedBy = MappingUtil.Description.class)
    @Mapping(expression = "java(java.util.Collections.emptyList())", target = "relatedResources")
    Skill toSkill(Map<Object, Object> map);
}
