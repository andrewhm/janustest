package com.janusprototype.janustest.repositories;

import com.janusprototype.janustest.domain.Contacts;
import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.mapper.ContactMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.springframework.stereotype.Repository;

import java.util.Map;

import static com.janusprototype.janustest.utils.Utils.EMAIL;
import static com.janusprototype.janustest.utils.Utils.INV;
import static com.janusprototype.janustest.utils.Utils.LABEL;
import static com.janusprototype.janustest.utils.Utils.MOBILEPHONE;
import static com.janusprototype.janustest.utils.Utils.OUTV;
import static com.janusprototype.janustest.utils.Utils.WORKPHONE;

@Repository
@Slf4j
@RequiredArgsConstructor
public class ContactRepository {
    private final GraphTraversalSource graphTraversalSource;
    private final Bindings b = Bindings.instance();

    public boolean create(Contacts contact) {
        try {
            graphTraversalSource.addV(b.of(LABEL, "contact"))
                    .property(EMAIL, b.of(EMAIL, contact.getEmail()))
                    .property(WORKPHONE, b.of(WORKPHONE, contact.getWorkPhone()))
                    .property(MOBILEPHONE, b.of(MOBILEPHONE, contact.getMobilePhone())).next();
        } catch (Exception e) {
            log.error("Error was happened while adding contact", e);
            return false;
        }
        log.info("Contact was added to db");
        return true;
    }

    public Contacts findContactById(Long id) {
        GraphTraversal graphTraversal = graphTraversalSource.V()
                .hasId(id)
                .valueMap();
        if (!graphTraversal.hasNext()) {
            log.info("data wasn't found in db");
            return null;
        }
        return ContactMapper.MAPPER.toContacts((Map<Object, Object>) graphTraversal.next());
    }

    public Boolean remove(Long id) {
        try {
            graphTraversalSource.V()
                    .hasId(id)
                    .drop().iterate();
        } catch (Exception e) {
            log.error("Error was happened while removing contact", e);
            return false;
        }
        log.info("Contact was removed");
        return true;
    }

    public Boolean update(Contacts contact) {
        try {
            graphTraversalSource.V()
                    .has("email", contact.getEmail())
                    .property(WORKPHONE, b.of(WORKPHONE, contact.getWorkPhone()))
                    .property(MOBILEPHONE, b.of(MOBILEPHONE, contact.getMobilePhone())).next();
        } catch (Exception e) {
            log.error("Error was happened while updating contact", e);
            return false;
        }
        log.info("Contact was updated");
        return true;
    }

    public Boolean addContactToEmployee(String email, Employee employee) {
        try {

            Vertex employeeVertex = graphTraversalSource.V()
                    .has("name", employee.getName()).next();


            Vertex contactVertex = graphTraversalSource.V()
                    .has("email", email).next();


            graphTraversalSource.V(b.of(OUTV, employeeVertex)).as("e").V(b.of(INV, contactVertex)).addE(b.of(LABEL, "contacts")).from("e").next();

        } catch (Exception e) {
            log.error("Error was happened while linking contact with employee", e);
            return false;
        }
        log.info("Link of contact was added to employee");
        return true;
    }
}
