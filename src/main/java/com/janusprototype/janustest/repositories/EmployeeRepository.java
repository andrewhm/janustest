package com.janusprototype.janustest.repositories;

import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.domain.Skill;
import com.janusprototype.janustest.mapper.EmployeeMapper;
import com.janusprototype.janustest.mapper.SkillMapper;
import com.janusprototype.janustest.repositories.filter.FilterMap;
import com.janusprototype.janustest.repositories.filter.FilterStrategy;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.Traversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.step.util.WithOptions;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

import static com.janusprototype.janustest.utils.Utils.CITY;
import static com.janusprototype.janustest.utils.Utils.DEPARTMENT;
import static com.janusprototype.janustest.utils.Utils.LABEL;
import static com.janusprototype.janustest.utils.Utils.NAME;
import static com.janusprototype.janustest.utils.Utils.POSITION;
import static org.apache.tinkerpop.gremlin.groovy.jsr223.dsl.credential.__.or;
import static org.apache.tinkerpop.gremlin.process.traversal.TextP.containing;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.has;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.identity;
import static org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__.union;

@RequiredArgsConstructor
@Repository
@Slf4j
public class EmployeeRepository {

    private final GraphTraversalSource graphTraversalSource;
    private final Bindings b = Bindings.instance();

    public boolean create(Employee employee) {
        try {
            graphTraversalSource.addV(b.of(LABEL, "employee"))
                    .property(NAME, b.of(NAME, employee.getName()))
                    .property(POSITION, b.of(POSITION, employee.getPosition()))
                    .property(DEPARTMENT, b.of(DEPARTMENT, employee.getDepartment()))
                    .property(CITY, b.of(CITY, employee.getCity())).next();
        } catch (Exception e) {
            log.error("Error was happened while adding employee", e);
            return false;
        }
        log.info("Employee was added to db");
        return true;
    }

    public Employee findEmployeeByName(String name) {
        GraphTraversal graphTraversal = graphTraversalSource.V()
                .has("name", containing(name))
                .valueMap().with(WithOptions.tokens);
        if (!graphTraversal.hasNext()) {
            log.info("data wasn't found in db");
            return null;
        }

        return EmployeeMapper.MAPPER.toEmployee((Map<Object, Object>) graphTraversal.next());
    }

    public Boolean remove(Long id) {
        try {
            graphTraversalSource.V()
                    .hasId(id)
                    .drop().iterate();
        } catch (Exception e) {
            log.error("Error was happened while removing employee", e);
            return false;
        }
        log.info("Employee was removed");
        return true;
    }

    public Boolean update(Employee employee) {
        try {
            graphTraversalSource.V()
                    .has("name", employee.getName())
                    .property(POSITION, b.of(POSITION, employee.getPosition()))
                    .property(DEPARTMENT, b.of(DEPARTMENT, employee.getDepartment()))
                    .property(CITY, b.of(CITY, employee.getCity())).next();
        } catch (Exception e) {
            log.error("Error was happened while updating employee", e);
            return false;
        }
        log.info("Employee was updated");
        return true;
    }

    public List<Skill> listSkills(Long id) {
        List<Skill> skillList = new LinkedList();
        GraphTraversal graphTraversal =  graphTraversalSource.V()
                .hasId(id)
                .out("skills").valueMap().with(WithOptions.tokens);
        while (graphTraversal.hasNext()) {
            skillList.add(SkillMapper.MAPPER.toSkill((Map<Object, Object>) graphTraversal.next()));
        }

        return skillList;
    }

    public List<Employee> findEmployeesWithContacts() {
        GraphTraversal graphTraversal = graphTraversalSource.V()
                .hasLabel("employee")
                .local(union(identity(), identity().out("contacts"))
                        .valueMap()
                        .with(WithOptions.tokens)
                        .fold());

        List<Employee> employeeList = new LinkedList<>();


        if (!graphTraversal.hasNext()) {
            log.info("data wasn't found in db");
            return employeeList;
        }

        while(graphTraversal.hasNext()) {
            employeeList.add(EmployeeMapper.MAPPER.toEmployeeWithInnerEntities((List<Map<Object, Object>>) graphTraversal.next()));
        }

        return employeeList;
    }

    public List<Employee> listBy(FilterMap filter) {
        GraphTraversal graphTraversal = graphTraversalSource.V();

        for(Map.Entry<String, List<Predicate>> entry: filter.getParameters().entrySet()) {
            List<Traversal<Object, Object>> traversals = new ArrayList<>();
            for(Predicate predicate: entry.getValue()) {
                traversals.add(has(entry.getKey(), predicate));
            }
            graphTraversal = graphTraversal.and(or(traversals.toArray(new Traversal[]{})));
        }

        graphTraversal.valueMap().with(WithOptions.tokens);

        List<Employee> employeeList = new LinkedList<>();
        while(graphTraversal.hasNext()) {
            employeeList.add(EmployeeMapper.MAPPER.toEmployee((Map<Object, Object>) graphTraversal.next()));
        }
        return employeeList;
    }

    public List<Employee> listBy(FilterStrategy filter) {
        GraphTraversal graphTraversal = graphTraversalSource.V();
        graphTraversal = filter.handle(graphTraversal).valueMap().with(WithOptions.tokens);

        List<Employee> employeeList = new LinkedList<>();
        while(graphTraversal.hasNext()) {
            employeeList.add(EmployeeMapper.MAPPER.toEmployee((Map<Object, Object>) graphTraversal.next()));
        }
        return employeeList;
    }
}
