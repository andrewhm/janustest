package com.janusprototype.janustest.repositories;

import com.janusprototype.janustest.domain.Employee;
import com.janusprototype.janustest.domain.Skill;
import com.janusprototype.janustest.mapper.SkillMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.process.traversal.Bindings;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.step.util.WithOptions;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.springframework.stereotype.Repository;

import java.util.Map;

import static com.janusprototype.janustest.utils.Utils.DESCRIPTION;
import static com.janusprototype.janustest.utils.Utils.INV;
import static com.janusprototype.janustest.utils.Utils.LABEL;
import static com.janusprototype.janustest.utils.Utils.OUTV;
import static com.janusprototype.janustest.utils.Utils.TITLE;

@Repository
@Slf4j
@RequiredArgsConstructor
public class SkillRepository {
    private final GraphTraversalSource graphTraversalSource;
    private final Bindings b = Bindings.instance();

    public boolean create(Skill skill) {
        try {
            graphTraversalSource.addV(b.of(LABEL, "skill"))
                    .property(TITLE, b.of(TITLE, skill.getTitle()))
                    .property(DESCRIPTION, b.of(DESCRIPTION, skill.getDescription())).next();
        } catch (Exception e) {
            log.error("Error was happened while adding skill", e);
            return false;
        }
        log.info("Skill was skill to db");
        return true;
    }

    public Skill findSkillByTitle(String title) {
        GraphTraversal graphTraversal = graphTraversalSource.V()
                .has(TITLE, title)
                .valueMap().with(WithOptions.tokens);
        if (!graphTraversal.hasNext()) {
            log.info("data wasn't found in db");
            return null;
        }

        return SkillMapper.MAPPER.toSkill((Map<Object, Object>) graphTraversal.next());
    }

    public Boolean remove(Long id) {
        try {
            graphTraversalSource.V()
                    .hasId(id)
                    .drop().iterate();
        } catch (Exception e) {
            log.error("Error was happened while removing skill", e);
            return false;
        }
        log.info("Skill was removed");
        return true;
    }

    public Boolean update(Skill skill) {
        try {
            graphTraversalSource.V()
                    .has("title", skill.getTitle())
                    .property(DESCRIPTION, b.of(DESCRIPTION, skill.getDescription())).next();
        } catch (Exception e) {
            log.error("Error was happened while updating skill", e);
            return false;
        }
        log.info("Skill was updated");
        return true;
    }

    public Boolean addSkillToEmployee(String title, Employee employee) {
        try {
            //Нахождение в БД объекта сотрудника по свойству "name"
            Vertex employeeVertex = graphTraversalSource.V()
                    .has("name", employee.getName()).next();

            //Нахождение в БД объекта скилла по свойству "title"
            Vertex skillVertex = graphTraversalSource.V()
                    .has("title", title).next();

            //Установление связи от сотрудника к скиллу посредством ребра с меткой "skills"
            graphTraversalSource.V(b.of(OUTV, employeeVertex)).as("e").V(b.of(INV, skillVertex)).addE(b.of(LABEL, "skills")).from("e").next();

        } catch (Exception e) {
            log.error("Error was happened while linking skill with employee", e);
            return false;
        }
        log.info("Link of skill was added to employee");
        return true;
    }
}
