package com.janusprototype.janustest.repositories.filter;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;

import static org.apache.tinkerpop.gremlin.groovy.jsr223.dsl.credential.__.has;
import static org.apache.tinkerpop.gremlin.process.traversal.P.eq;
import static org.apache.tinkerpop.gremlin.process.traversal.TextP.containing;

public class FilterEmployeeByNameCityPosition implements FilterStrategy {
    @Override
    public GraphTraversal handle(GraphTraversal graphTraversal) {
        return graphTraversal
                .or(has("name", containing("Jack")),
                    has("name", containing("Иван")))
                .and(has("city", eq("Москва")))
                .and(has("position", containing("разработчик")));
    }
}
