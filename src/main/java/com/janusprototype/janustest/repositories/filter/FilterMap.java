package com.janusprototype.janustest.repositories.filter;


import lombok.Getter;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

@Getter
public class FilterMap {
    private Map<String, List<Predicate>> parameters = new HashMap<>();

    public void setParameter(String key, List<Predicate> predicates) {
        parameters.put(key, predicates);
    }
}
