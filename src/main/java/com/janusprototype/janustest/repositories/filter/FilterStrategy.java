package com.janusprototype.janustest.repositories.filter;

import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversal;

public interface FilterStrategy {
    GraphTraversal handle(GraphTraversal graphTraversal);
}
