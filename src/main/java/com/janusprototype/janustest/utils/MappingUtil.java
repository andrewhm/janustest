package com.janusprototype.janustest.utils;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.tinkerpop.gremlin.structure.T;
import org.mapstruct.Qualifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
public class MappingUtil {

    private static final String ERROR_HAPPENED = "Error happened while type casting";

    public enum Property {
        TITLE("title"),
        NAME("name"),
        POSITION("position"),
        DEPARTMENT("department"),
        CITY("city"),
        EMAIL("email"),
        MOBILEPHONE("mobile_phone"),
        WORKPHONE("work_phone"),
        DESCRIPTION("description");

        @Getter
        private String description;

        Property(String description) {
            this.description = description;
        }

    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Title {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Id {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Description {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Name {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Position {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Department {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface City {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface Email {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface MobilePhone {
    }

    @Qualifier
    @Target(ElementType.METHOD)
    @Retention(RetentionPolicy.SOURCE)
    public static @interface WorkPhone {
    }

    @Title
    public String title(Map<Object, Object> in) {
        return extractValue(in, Property.TITLE);
    }

    @Description
    public String description(Map<Object, Object> in) {
        return extractValue(in, Property.DESCRIPTION);
    }

    @Position
    public String position(Map<Object, Object> in) {
        return extractValue(in, Property.POSITION);
    }

    @Department
    public String department(Map<Object, Object> in) {
        return extractValue(in, Property.DEPARTMENT);
    }

    @City
    public String city(Map<Object, Object> in) {
        return extractValue(in, Property.CITY);
    }

    @Id
    public Long id(Map<Object, Object> in) {
        return extractValue(in, T.id);
    }

    @Name
    public String name(Map<Object, Object> in) {
        return extractValue(in, Property.NAME);
    }

    @Email
    public String email(Map<Object, Object> in) {
        return extractValue(in, Property.EMAIL);
    }

    @WorkPhone
    public String workphone(Map<Object, Object> in) {
        return extractValue(in, Property.WORKPHONE);
    }

    @MobilePhone
    public String mobilephone(Map<Object, Object> in) {
        return extractValue(in, Property.MOBILEPHONE);
    }


    private <E> E extractValue(Map<Object, Object> in, Property key) {
        try {
            return (E) Optional.ofNullable(in)
                    .map(map -> (List) map.get(key.getDescription()))
                    .map(list -> list.get(0))
                    .orElse(null);
        } catch (ClassCastException e) {
            log.warn(ERROR_HAPPENED, e);
        }
        return null;
    }

    private <E> E extractValue(Map<Object, Object> in, org.apache.tinkerpop.gremlin.structure.T key) {
        try {
            return (E) Optional.ofNullable(in)
                    .map(map -> map.get(key))
                    .orElse(null);
        } catch (ClassCastException e) {
            log.warn(ERROR_HAPPENED, e);
        }
        return null;
    }
}
