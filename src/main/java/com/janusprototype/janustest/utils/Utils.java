package com.janusprototype.janustest.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Utils {

    public static final String LABEL = "label";
    public static final String NAME = "name";
    public static final String POSITION = "position";
    public static final String DEPARTMENT = "department";
    public static final String CITY = "city";
    public static final String EMAIL = "email";
    public static final String WORKPHONE = "work_phone";
    public static final String MOBILEPHONE = "mobile_phone";
    public static final String EDUCATIONINSTITUTION = "education_institution";
    public static final String SPECIALIZATION = "specialization";
    public static final String DEGREE = "degree";
    public static final String STARTEDAT = "started_at";
    public static final String ENDEDAT = "ended_at";
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    public static final String PASSEDAT = "passed_at";
    public static final String DESCRIPTION = "description";
    public static final String OUTV = "outV";
    public static final String INV = "inV";

    public static String createSchemaRequest() {
        final StringBuilder s = new StringBuilder();

        s.append("JanusGraphManagement management = graph.openManagement(); ");
        s.append("boolean created = false; ");

        // naive check if the schema was previously created
        s.append(
                "if (management.getRelationTypes(RelationType.class).iterator().hasNext()) { management.rollback(); created = false; } else { ");

        // properties
        s.append("PropertyKey name = management.makePropertyKey(\"name\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"position\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"department\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"city\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"email\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"work_phone\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"mobile_phone\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"education_institution\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"specialization\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"degree\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"started_at\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"ended_at\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"title\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"type\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"passed_at\").dataType(String.class).make(); ");
        s.append("management.makePropertyKey(\"description\").dataType(String.class).make(); ");

        // vertex labels
        s.append("management.makeVertexLabel(\"employee\").make(); ");
        s.append("management.makeVertexLabel(\"contact\").make(); ");
        s.append("management.makeVertexLabel(\"education\").make(); ");
        s.append("management.makeVertexLabel(\"studies\").make(); ");
        s.append("management.makeVertexLabel(\"skill\").make(); ");

        // edge labels
        s.append("management.makeEdgeLabel(\"contacts\").make(); ");
        s.append("management.makeEdgeLabel(\"educations\").make(); ");
        s.append("management.makeEdgeLabel(\"studies\").make(); ");
        s.append("management.makeEdgeLabel(\"skills\").make(); ");

        // composite indexes
        s.append("management.buildIndex(\"nameIndex\", Vertex.class).addKey(name).buildCompositeIndex(); ");

        s.append("management.commit(); created = true; }");

        return s.toString();
    }

}
